"use strict";

let texts = ["Zlatá miska", "Namaskar", "SuChi", "Zdravý život"]; //, "Naše Vaše Bistro", "Garden", '1', '4', '5', '6'];//, '7','8','9', '1', '1', '1', '1'];

document.getElementById("restaurants").onsubmit = (e) => {
  e.preventDefault();
  const formData = new FormData(e.target);

  let restaurants = [];
  for (let data of formData.entries()) {
    if (data[0] == "restaurant") {
      restaurants.push(data[1]);
    }
    if (data[0] == "other") {
      const otherRestaurants = data[1].split("\n");
      for (let r of otherRestaurants) {
        r = r.trim();
        if (r != "") {
          restaurants.push(r);
        }
      }
    }
  }
  if (restaurants.length < 1) {
    alert("Nelze vybírat restauraci bez výběru restaurace.");
  } else if (restaurants.length > 10) {
    alert("Maximálně 10 možností, víc nedám.");
  } else {
    makeWheel(restaurants);
    document.getElementById("wheel-container").style.display = "block";
    document.getElementById("restaurants").style.display = "none";
  }
};

function makeWheel(texts) {
  const originColors = [
    "orchid-orange",
    "spiro-disco-ball",
    "honey-glow",
    "sweet-garden",
    "falling-star",
    "rich-gerdenia",
    "clear-chill",
    "sarawak-white-pepper",
    "keppel",
    "ships-officer",
    "fiery-fuchsia",
    "bluebell",
    "georgia-peach",
    "oasis-stream",
    "bright-ube",
    "magenta-purple",
    "ending-navy-blue",
    "sasquatch-socks",
    "pine-glade",
    "highlighter-lavender",
  ];

  // Generator of random integer between two values (only for non negative ints).
  const randomNumber = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

  // Transform sizes (@todo: make it matematically good for larger scales)
  const sizes = {
    4: 0,
    6: 21,
    8: 29.2,
    10: 33.7,
    12: 36.0,
    14: 38.57,
    16: 40.05,
    18: 41.15,
  };

  document.getElementById("spin-button").style.display = "block";

  // Initiate color choosing function.
  let wheelTextsWithColors = [];
  let colors = [];
  for (let i = 0; i < texts.length; i++) {
    if (!colors.length) {
      colors = [...originColors];
    }

    const color = colors.splice(randomNumber(1, colors.length) - 1, 1);
    wheelTextsWithColors.push([texts[i], color]);
  }

  // There need to be odd number of items larger than 2. If not duplicate the list.
  while (wheelTextsWithColors.length < 3 || wheelTextsWithColors.length % 2 == 1) {
    wheelTextsWithColors = wheelTextsWithColors.concat(wheelTextsWithColors);
  }

  // Empty the wheel segments and add new options.

  const wheel = document.getElementById("wheel");
  wheel.innerHTML = "";
  wheel.style.transform = "";
  wheel.style.transition = "transform 5s";
  if (texts.length > 10) {
    alert("Kolo štěstí s víc než 10 možnostmi neumí pracovat, sorry.");
  } else {
    for (let i = 0; i < wheelTextsWithColors.length; i++) {
      let option = document.createElement("div");
      option.innerHTML = "<span>" + wheelTextsWithColors[i][0] + "</span>";
      option.style.transform = "rotate(" + i * (360 / wheelTextsWithColors.length) + "deg)";
      option.style.clipPath = "polygon(50% 100%, " + sizes[wheelTextsWithColors.length] + "% 0 , " + (100 - sizes[wheelTextsWithColors.length]) + "% 0 )";
      option.classList.add("color-" + wheelTextsWithColors[i][1]);
      option.classList.add("segment");
      wheel.appendChild(option);
    }
  }

  // Rotate the wheel
  document.getElementById("spin-button").onclick = (e) => {
    e.preventDefault();
    const selectedRestaurant = randomNumber(0, wheelTextsWithColors.length - 1);
    const totalRotation = 360 * randomNumber(3, 7) + selectedRestaurant * (360 / wheelTextsWithColors.length);
    wheel.style.transform = "rotate(" + totalRotation + "deg)";
    document.getElementById("spin-button").style.display = "none";
  };
}
